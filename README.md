Risketos
Han d’haver dos jugadors com a mínim. Els dos jugadors han de ser diferents i incorporar:
    
    • Hitbox i Hurtbox
    • Esto se puede encontrar en los GameObjects P1 y P2

    • Barra de vida o similar (que es tindrà actualitzada per delegats)
    • Se puede ver solo entrar en las escenas de mapa

    • Les animacions han de moure les hitboxes i hurtboxes corresponents
    • Si se entra a las animaciones de ataque podemos ver como se hace esto

    • Les colisions tenen que estar correctament implementades, amb opció de bloqueig.
    • Hay bloqueo en los 2 personajes

    • Els impactes han de tenir reaccions físiques (haura de sortir rebutjat, o recular, o caure)
    • Hay un ataque que hace KnockUp en cada personaje

    • Possibles combos, controlats per temps amb corrutines
    • Hay un combo en cada pj

    • Projectils (amb físiques complexes, que no vagin en linea recta)
    • El combo és un proyectil

    • Efectes de particles
    • Al tocar el proyectil con el pj hace un efecto de particulas

Risketos Addicionals
    
    • Animator amb Frame by Frame
    • Se puede ver solo iniciar el mapa

    • Efectes d’audio i vídeo
    • Hay pistas de audio en todas las escenas

    • Ús d’Effectors
    • Effectors en el mapa 2, el de pruebas

    • Ús de Joints
    • La lampara del mapa 2 se puede destruir el Joint
