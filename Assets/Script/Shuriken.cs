using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shuriken : MonoBehaviour
{
    public GameObject hitEffect;
    Animator anim;

    private void Start()
    {
        //Inicialicamos el animator
        anim = GetComponent<Animator>();
        anim.Play("Shuriken");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Si colisiona con el trigger del p2, se destruye y activa el efecto de particulas
        if (collision.transform.tag.Equals("eneHurtboxTrigger"))
        {
            Destroy(this.gameObject);
            GameObject effect = Instantiate(hitEffect, transform.position, Quaternion.identity);
        }
        //Si collisiona con el suelo se destruye
        else if (collision.transform.tag.Equals("suelo"))
        {
            Destroy(this.gameObject);
        }


    }
}
