using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TheSceneManager : MonoBehaviour
{
    //Ponemos escena de seleccion de mapa
    public void SelectMap()
    {
        SceneManager.LoadScene("MapSelec");
    }

    //Ponemos escena del mapa 1
    public void SelectMap1()
    {
        SceneManager.LoadScene("SampleScene");
    }

    //Ponemos escena del mapa 2
    public void SelectMap2()
    {
        SceneManager.LoadScene("WithEffectors");
    }   
}
