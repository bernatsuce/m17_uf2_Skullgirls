using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class P2 : MonoBehaviour
{
    [SerializeField] private PJSO m_ene;

    public Transform firePoint;

    private Rigidbody2D rb;
    private GameObject player;
    Animator anim;

    public Image circleBar;
    public Image extraBar;
    public float circlePercent = 0.3f;
    private const float circleFillAmount = 0.75f;

    public float hp;
    private float vidaMax;
    private bool salto;
    private bool vivo = true;
    private int speed = 13;
    private bool Defensa = false;
    private int dirX = 0;
    private int comboStatus = 0;
    private GameObject pjProjectil;
    private float bulletForce;
    private float bulletVel;
    private bool cd;

    public GameObject hpcanvas;
    public GameObject hpcanvasenemy;
    public GameObject P1win;
    private void Awake()
    {
        //Ponemos las variables del SO a variables privadas
        vidaMax = m_ene.vidaMax;
        salto = m_ene.salto;
        vivo = m_ene.vivo;
        speed = m_ene.speed;
        Defensa = m_ene.Defensa;
        dirX = m_ene.dirX;
        comboStatus = m_ene.comboStatus;
        pjProjectil = m_ene.pjProjectil;
        bulletForce = m_ene.bulletForce;
        bulletVel = m_ene.bulletVel;
    }

    void Start()
    {
        //Activamos el canvas y seteamos la vida,
        hpcanvas.SetActive(true);
        hpcanvasenemy.SetActive(true);
        hp += vidaMax;

        //Buscamos si el player2 esta vivo con tag "ene"
        if (player == null)
        {
            player = GameObject.FindWithTag("ene");
        }

        //Inicialicamos el animator, el rigidbody y ponemos los Booleans como los queremos al inicio
        anim = GetComponent<Animator>();
        rb = this.GetComponent<Rigidbody2D>();
        vivo = true;
        salto = false;
    }

    void Update()
    {

        //Actualizamos hp
        CircleFill();
        ExtraFill();

        //Start combo
        if (Input.GetKeyDown("b"))
        {
            StartCoroutine(combo("b"));
        }
        //Segunda parte combo
        else if (Input.GetKeyDown("n"))
        {
            StartCoroutine(combo("n"));
        }
        //Tercera parte combo
        else if (Input.GetKeyDown("m"))
        {
            StartCoroutine(combo("m"));
        }

        //Si salto es true y le damos a la tecla up
        if (Input.GetKeyDown("up") && salto)
        {
            //Animacion + A�adimos fuerza al rb
            anim.Play("EneJump");
            rb.AddForce(new Vector2(0, 300));
            salto = false;
        }

        //Si salto es true y le damos a la tecla up+left
        else if (Input.GetKeyDown("up") && Input.GetKeyDown("left") && salto)
        {
            //Animacion + A�adimos fuerza al rb
            anim.Play("EneJump");
            rb.AddForce(new Vector2(0, 300));
            salto = false;
            rb.velocity = new Vector2(speed * -1, rb.velocity.y);

        }

        //Si salto es true y le damos a la tecla up+right
        else if (Input.GetKeyDown("up") && Input.GetKeyDown("right") && salto)
        {
            //Animacion + A�adimos fuerza al rb
            anim.Play("EneJump");
            rb.AddForce(new Vector2(0, 300));
            salto = false;
            rb.velocity = new Vector2(speed, rb.velocity.y);
        }

        //Si le damos a la tecla down
        else if (Input.GetKey("down"))
        {

            //A�adimos fuerza negativa al rb
            rb.AddForce(new Vector2(0, -20));
        }

        //Si salto es true y le damos a la tecla left
        else if (Input.GetKeyDown("left") && salto)
        {
            //Animacion + A�adimos velocidad al rb
            anim.Play("EneWalkForward");
            rb.AddForce(new Vector2(0, 0));
            rb.velocity = new Vector2(speed * -1, rb.velocity.y);
            dirX = -speed;
        }

        //Si salto es true y le damos a right
        else if (Input.GetKeyDown("right") && salto)
        {
            //Animacion + A�adimos velocidad al rb
            anim.Play("EneWalkBack");
            rb.AddForce(new Vector2(0, 0));
            rb.velocity = new Vector2(speed, rb.velocity.y);
            dirX = speed;
        }


        //ESTO ES LO MISMO DE ANTES SIN ANIMACION Y SIN BOOLEAN PARA QUE TE PUEDAS MOVER EN EL AIRE
        else if (Input.GetKey("left"))
        {
            rb.AddForce(new Vector2(0, 0));
            rb.velocity = new Vector2(speed * -1, rb.velocity.y);
            dirX = -speed;
        }

        else if (Input.GetKey("right"))
        {
            rb.AddForce(new Vector2(0, 0));
            rb.velocity = new Vector2(speed, rb.velocity.y);
            dirX = speed;
          
        }

        //Si esta vivo, no esta en cd y le damos a la h
        else if (Input.GetKeyDown("h") && vivo && !cd)
        {
            //Hacemos el ataque normal
            anim.Play("EneNormal");
            StartCoroutine(postAtkNormal());
            
        }

        //Si esta vivo, no esta en cd, le damos a la j, y el salto no esta disponible
        else if (Input.GetKeyDown("j") && vivo && !salto && !cd)
        {
            //Hacemos el ataque medio(salto)
            anim.Play("EneMid");
            StartCoroutine(cdForMid());
        }

        //Si esta vivo, no esta en cd y le damos a la j
        else if (Input.GetKeyDown("k") && vivo && !cd)
        {
            //Hacemos el ataque Alto
            anim.Play("EneAlt");
            StartCoroutine(postAtkKnock());
        }

        //Si esta vivo, el salto esta disponible y le damos a la l, bloqueamos
        else if (Input.GetKey("l") && vivo && salto)
        {
            anim.Play("EnePostBlock");
            Defensa = true;
            //StartCoroutine(postBlock());
        }
        //Si esta vivo, el salto esta disponible y le dejamos de presionar a la l, dejamos de bloqueamos
        else if (!Input.GetKey("l") && vivo && salto)
        {
            Defensa = false;
            //anim.Play("EneIdle");
        }

        //Si esta vivo, el salto no esta disponible y le damos a la l, bloqueamos
        else if (Input.GetKey("l") && vivo && !salto)
        {
            anim.Play("EnePostBlock");
            Defensa = true;
            //StartCoroutine(postBlock());
        }
        //Si esta vivo, el salto no esta disponible y le dejamos de presionar a la l, dejamos de bloqueamos
        else if (!Input.GetKey("l") && vivo && !salto)
        {
            Defensa = false;
           // anim.Play("EneIdle");
        }

        //Si la direccion �s 0
        else if (dirX == 0)
        {
            //Animacion idle
            anim.Play("EneIdle");
        }
        else
        {
            //si no hace nada, la direccion es 0
            dirX = 0;
        }

        //Si el hp de personaje llega a 0
        if (hp <= 0)
        {
            //Actualizamos el hp, desactivamos el hp, activamos el win del p1, desactivamos el personaje y lo destruimos
            CircleFill();
            ExtraFill();
            hpcanvas.SetActive(false);
            hpcanvasenemy.SetActive(false);
            P1win.SetActive(true);
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
    }

    void CircleFill()
    {
        //Funcionalidad de la zona circular de el hp
        float healthPercent = hp / vidaMax;
        float circleFill = healthPercent / circlePercent;
        circleFill *= circleFillAmount;
        circleFill = Mathf.Clamp(circleFill, 0, circleFillAmount);
        circleBar.fillAmount = circleFill;
    }

    void ExtraFill()
    {
        //Funcionalidad de la parte recta del hp
        float circleAmount = circlePercent * vidaMax;
        float extraHealth = hp - circleAmount;
        float extraTotalHealth = vidaMax - circleAmount;

        float extraFill = extraHealth / extraTotalHealth;
        extraFill = Mathf.Clamp(extraFill, 0, 1);
        extraBar.fillAmount = extraFill;
    }

    IEnumerator postAtkNormal()
    {
        //Despues de un ataque normal esperamos, activamos el Idle y ponemos cd
        yield return new WaitForSeconds(1.4f);
        anim.Play("EneIdle");
        print("Reinicio");
        cd = true;
        yield return new WaitForSeconds(1f);
        cd = false;
    }

    IEnumerator postAtkKnock()
    {
        //Despues de un ataque de KnockUP esperamos, activamos el Idle y ponemos cd
        yield return new WaitForSeconds(0.6f);
        anim.Play("EneIdle");
        print("Reinicio");
        cd = true;
        yield return new WaitForSeconds(1f);
        cd = false;
    }
    IEnumerator postBlock()
    {
        //Despues de un bloqueo esperamos, activamos el Idle y ponemos cd
        yield return new WaitForSeconds(1f);
        anim.Play("EnePostBlock");
        print("Reinicio");
        cd = true;
        yield return new WaitForSeconds(1f);
        cd = false;
    }

    IEnumerator cdForMid()
    {

        //Despues de un ataque medio ponemos cd
        cd = true;
        yield return new WaitForSeconds(1f);
        cd = false;
    }

    IEnumerator combo(string s)
    {
        //Si le damos a b
        if (s == "b")
        {
            //Parte 1 del combo (Info)
            if (comboStatus == 0)
            {
                print("combo1");

                comboStatus = 1;
                yield return new WaitForSeconds(0.5f);
                if (comboStatus == 1)
                {
                    print("combo fallado por tiempo");
                    comboStatus = 0;
                }
            }
            else
            {
                comboStatus = 0;
                print("tecla pulsada a destiempo");
            }

        }
        //Si le damos a la n
        //Parte 2 del combo (Info)
        else if (s == "n")
        {
            if (comboStatus == 1)
            {
                print("combo2");
                comboStatus = 2;
                yield return new WaitForSeconds(0.5f);
                if (comboStatus == 2)
                {
                    print("combo fallado por tiempo");
                    comboStatus = 0;
                }
            }
            else
            {
                comboStatus = 0;
                print("tecla pulsada a destiempo");
            }
        }
        //Si le damos a la m
        //Parte 3 del combo, activamos la animacion de throw, lanzamos el proyectil, y cuando acabe Idle
        else if (s == "m")
        {
            if (comboStatus == 2)
            {
                print("combo3");
                comboStatus = 3;
                print("anima");
                anim.Play("EneThrow");
                StartCoroutine(shooting());
                StartCoroutine(postThrow());
                
                print("Combo completo");
                comboStatus = 0;

                yield return new WaitForSeconds(0.5f);
                if (comboStatus == 0)
                {

                }
            }
            else
            {
                comboStatus = 0;
                print("tecla pulsada a destiempo");
            }
        }
        else
        {
            comboStatus = 0;
            print("tecla incorrecta");
        }
        SetAnimationState();
    }

    IEnumerator postThrow()
    {
        //Despues de acabar el lanzamiento, ponemos el Idle
        yield return new WaitForSeconds(0.7f);
        anim.Play("EneIdle");
        print("Reinicio");
    }

    IEnumerator shooting()
    {
        //Disparamos
        yield return new WaitForSeconds(0.25f);
        Shoot();
    }

    void Shoot()
    {
        //Como funciona el disparo, como se instancia y le damos fuerza
        GameObject bullet = Instantiate(pjProjectil, firePoint.transform.position, firePoint.transform.rotation);
        print(firePoint.position);
        Destroy(bullet, 10f);
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(new Vector2(-bulletVel, bulletForce));
        rb.velocity = new Vector2(speed * -1, rb.velocity.y);
        print("Disparado");

    }

    void SetAnimationState()
    {
        //Al acabar el shoot, depende de lo que hagamos, activara algo
        if (dirX == 0)
        {
            anim.Play("EneIdle");
        }
        else if (Input.GetKey("left"))
        {
            anim.Play("EneWalkForward");
            //A�adimos la fuerza a rb
            rb.AddForce(new Vector2(0, 0));
            rb.velocity = new Vector2(speed * -1, rb.velocity.y);
            dirX = -speed;
        }

        else if (Input.GetKey("right"))
        {
            anim.Play("EneWalkBack");
            rb.AddForce(new Vector2(0, 0));
            rb.velocity = new Vector2(speed, rb.velocity.y);
            dirX = speed;
        }

    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        //Si no te estas defendiendo
        if (!Defensa)
        {
            //Efectos del ataque normal de p1
            if (col.tag.Equals("normal"))
            {
                hp -= 10f;
                print("ola");
                //Si llega a 0
                if (hp <= 0)
                {
                    //Actualizamos el hp, desactivamos el hp, activamos el win del p1, desactivamos el personaje y lo destruimos
                    CircleFill();
                    ExtraFill();
                    hpcanvas.SetActive(false);
                    hpcanvasenemy.SetActive(false);
                    P1win.SetActive(true);
                    gameObject.SetActive(false);
                    Destroy(gameObject);
                }
            }
            //Efectos del ataque medio de p1
            if (col.tag.Equals("mid"))
            {
                hp -= 20f;
                print("ola");
                //Si llega a 0
                if (hp <= 0)
                {
                    //Actualizamos el hp, desactivamos el hp, activamos el win del p1, desactivamos el personaje y lo destruimos
                    CircleFill();
                    ExtraFill();
                    hpcanvas.SetActive(false);
                    hpcanvasenemy.SetActive(false);
                    P1win.SetActive(true);
                    gameObject.SetActive(false);
                    Destroy(gameObject);
                }
            }
            //Efectos del proyectil de p1
            if (col.tag.Equals("project"))
            {
                hp -= 30f;
                print("ola");
                //Si llega a 0
                if (hp <= 0)
                {
                    //Actualizamos el hp, desactivamos el hp, activamos el win del p1, desactivamos el personaje y lo destruimos
                    CircleFill();
                    ExtraFill();
                    hpcanvas.SetActive(false);
                    hpcanvasenemy.SetActive(false);
                    P1win.SetActive(true);
                    gameObject.SetActive(false);
                    Destroy(gameObject);
                }
            }

            //Efectos del ataque KnockUp de p1
            if (col.tag.Equals("Alt"))
            {
                hp -= 20f;
                print("ola");
                rb.AddForce(new Vector2(0, 400));
                salto = false;
                rb.velocity = new Vector2(speed * -1, rb.velocity.y);
                anim.Play("EneKnockUp");
                StartCoroutine(postforce());
                //Si llega a 0
                if (hp <= 0)
                {
                    //Actualizamos el hp, desactivamos el hp, activamos el win del p1, desactivamos el personaje y lo destruimos
                    CircleFill();
                    ExtraFill();
                    hpcanvas.SetActive(false);
                    hpcanvasenemy.SetActive(false);
                    P1win.SetActive(true);
                    gameObject.SetActive(false);
                    Destroy(gameObject);
                }
            }
        }
    }


    IEnumerator postforce()
    {
        //Despues de que te de un KnopckUp, ponemos las animaciones
        yield return new WaitForSeconds(0.5f);
        anim.Play("EneKnockDown");
        yield return new WaitForSeconds(0.4f);
        //Para que no se abuse si te esta levantando, activamos animacion y ponemos defensa
        if (salto)
        {
            Defensa = true;
            anim.Play("EneWakeup");
            yield return new WaitForSeconds(0.7f);
            anim.Play("EneIdle");
            Defensa = false;
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Si toca el suelo
        if (collision.gameObject.tag == "suelo")
        {
            //Activamos salto y ponemos animacion Idle
            //OLE MI BETIIIIIIIIS (Solo para comprobar)
            print("ole betis");
            salto = true;
            anim.Play("EneIdle");
        }
    }
}
