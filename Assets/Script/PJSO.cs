using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PJSO : ScriptableObject
{
    //atributos de los personajes
    public float vidaMax = 250f;
    public bool salto;
    public bool vivo = true;
    public int speed = 13;
    public bool Defensa = false;
    public int dirX = 0;
    public int comboStatus = 0; 
    public GameObject pjProjectil;
    public float bulletForce;
    public float bulletVel;
}
